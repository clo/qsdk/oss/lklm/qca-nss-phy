obj-m += $(TARGET).o

$(TARGET)-objs := \
		nss_phy_common.o	\
		nss_phy.o	\
		nss_phy_linux_wrapper.o	\
		nss_phy_linux.o	\
		qca807x_phy.o	\
